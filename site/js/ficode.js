$(window).load(function() {
	detectLanguage();
});
activeLink();
corregirActive();
function activeLink() {
	var path = window.location.href.substring(31);
	// Páginas dentro de Soluciones
	if (path == 'pigknows.html' || path == 'abi.html' || path == 'csd.html' || path == 'pkapp.html' ||
		path == 'pigknows.html#home' || path == 'abi.html#home' || path == 'csd.html#home' || path == 'pkapp.html#home') {
		link = '#home';
	}
	else{
		link = path;
	}
	$('.rd-nav-link[href="'+link+'"]').parent('.rd-nav-item').addClass('active');
}

// Corregimos subenlaces activados
function corregirActive() {
	$('.rd-nav-item').mouseenter(function() {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			$(this).addClass('actived');
		}
	}).mouseleave(function() {
		if ($(this).hasClass('actived')) {
			$(this).removeClass('actived');
			$(this).addClass('active');
		}
	});
}

//Geolocalización para codigo de región
function geoLocation(phone) {
	function getLocation() {
	    if (navigator.geolocation) {
	       navigator.geolocation.getCurrentPosition(positionCallback, positionFail);
	    }
	}

	function positionCallback(position) {
	    var geocoder = new google.maps.Geocoder;
	    var latlng = new google.maps.LatLng(
	        position.coords.latitude,
	        position.coords.longitude
	    );

	    geocoder.geocode({
	        latLng: latlng
	    }, function(results, status) {
	        if (status == google.maps.GeocoderStatus.OK) {
	            if (results[1]) {
	                // find only the city name
	                for (var i = 0; i < results[0].address_components.length; i++) {
	                    if (results[0].address_components[i].types[0] == "country") {
	                        check__country(results[0].address_components[i].short_name);
	                    }
	                }
	            }
	        }
	    });
	}
	function positionFail() {
		check__country('undefined');
	}
	function check__country(country) {
		regionCodeFormat(phone, country);
		writeRegionCode(phone);
	}
	getLocation();
}

function regionCodeFormat(phone, country) {
	if (country == 'undefined') {
		country = 'us';
	}
	phone.siblings('label').text('');
	var iti = phone.intlTelInput({
		utilsScript: "js/utils.js",
		initialCountry: country.toLowerCase(),
	});
}

function writeRegionCode(phone) {
	let initvalue = phone.val().substring(phone.val().lastIndexOf('-')+1); 
	let regioncode = phone.intlTelInput("getSelectedCountryData").dialCode;
	phone.val('');
	phone.val('+'+regioncode+'-'+initvalue);
}

function detectLanguage() {
	var ln = x=window.navigator.language.substring(0,2) || navigator.browserLanguage;
	console.log(ln);
	if(ln.substring(0,2) != 'es' && ln != 'pt-br'){
		window.location.href = "https://pigknows.com/";
	}
}

//Formulario de contacto
$('#contact-form-phone').focus(function(){
	geoLocation($(this));
}).change(function() {
	console.log($(this).val());
}).on('countrychange',function(){
	writeRegionCode($(this));
});

$('#contact-modal-phone').focus(function(){
	geoLocation($(this));
}).change(function() {
	console.log($(this).val());
}).on('countrychange',function(){
	writeRegionCode($(this));
});

$('#contact-form').submit(function(event) {
	event.preventDefault();
	var template_params = {
	   "name": $('#contact-form-name').val(),
	   "email": $('#contact-form-email').val(),
	   "phone": $('#contact-form-phone').val(),
	   "message": $('#contact-form-message').val()
	}
	var service_id = "gmail";
	var template_id = "contact_form";

	if (emailjs.send(service_id, template_id, template_params)) {
		alert('Mensaje enviado, pronto nos pondremos en contacto!');
		$('#contact-form-name').empty();
		$('#contact-form-email').empty();
		$('#contact-form-phone').empty();
		$('#contact-form-message').empty();

	}
	else{
		alert('Algo salío mal al enviar el correo. Revisa que los datos esten bien.');
	}
});

$('#contact-modal').submit(function(event) {
	event.preventDefault();
	var template_params = {
	   "name": $('#contact-modal-name').val(),
	   "email": $('#contact-modal-email').val(),
	   "phone": $('#contact-modal-phone').val(),
	   "message": $('#contact-modal-message').val()
	}
	var service_id = "gmail";
	var template_id = "contact_form";

	if (emailjs.send(service_id, template_id, template_params)) {
		alert('Mensaje enviado, pronto nos pondremos en contacto!');
		$('#contact-modal-name').empty();
		$('#contact-modal-email').empty();
		$('#contact-modal-phone').empty();
		$('#contact-modal-message').empty();
	}
	else{
		alert('Algo salío mal al enviar el correo. Revisa que los datos esten bien.');
	}
});